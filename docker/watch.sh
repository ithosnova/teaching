#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

watch() {
    echo watching folder $1/ every $2 secs.

while true
do
    find "$1" -type f -mtime -2s -print0 | while IFS= read -r -d '' line; do
        process "$line"
    done

    sleep 1
done
}

watch "$1" "$2"

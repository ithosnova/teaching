% 2D Toxicity MIA PaCa-2
% Iveta Terezie Hošnová
% 2022-12-21 to 2022-12-23

# Basic information

- nanoparticles: GGAG:Ce@SiO~2~, GGAG:Ce@SiO~2~-RB (new batch with RB, less pink colour)
- concentration: 0.0; 0.05; 0.5; 5.0; 50; 500; 1000 µg/mL
- other conditions: total killing with 10% DMSO
- cell growing time: 24 h
- incubation with NPs: 24 h
- MTS assay

## Description of each experiment

- [2022-12-21 2D Tox MIA PaCa-2 R1](https://notes.ivetaterezie.com/20221221-Tox2D-MIAPaCa2.html)  
- [2022-12-22 2D Tox MIA PaCa-2 R2](https://notes.ivetaterezie.com/20221222-Tox2D-MIAPaCa2.html)  
- [2022-12-23 2D Tox MIA PaCa-2 R3](https://notes.ivetaterezie.com/20221223-Tox2D-MIAPaCa2.html)

# Data for all three replications

![](20221221-23-Tox2D-MIAPaCa2-summary%2FMIAPaCa-2Dtox-R123-GGAGCeSiO2.png)  

![](20221221-23-Tox2D-MIAPaCa2-summary%2FMIAPaCa-2Dtox-R123-GGAGCeSiO2RB.png)
